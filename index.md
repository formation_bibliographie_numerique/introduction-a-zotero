# Introduction à Zotero
<small>Chaire de recherche du Canada sur les Écritures numériques<br>
Janvier 2020</small>

===d===

## 0. L’importance de structurer les références (bibliographiques)

- communauté scientifique & standards : inscrire votre travail dans les standards scientifiques d’une communauté&nbsp;;
- éthique : qui réfère bien respecte le travail cité&nbsp;;
- pérennité : pour retrouver vos sources – médiation d’un travail de sources à la base de votre recherche&nbsp;;
- esthétique : une bibliographie imparfaite fait tâche dans un travail de recherche&nbsp;;
- personnel : vous avez le choix de structurer ces références selon le type de style bibliographique.

===d===

## 1. Qu'est-ce que Zotero ?

===b===

## 1.1. Qu'est-ce que Zotero ?

- un logiciel et un service qui permet de gérer des références bibliographiques&nbsp;: conserver, organiser, citer et créer des bibliographies&nbsp;;
- un logiciel libre, et gratuit&nbsp;;
- un outil qui peut être collaboratif&nbsp;;
- une logiciel + une extension de navigateur pour récupérer des références.

===b===

## 1.2. Fonctionnalités

- *collecter*&nbsp;: collecter des références bibliographiques en ligne et les conserver&nbsp;;
- *organiser*&nbsp;: organiser ses références en collections et sous-collections, et en attribuant des tags et des marqueurs&nbsp;;
- *citer*&nbsp;: citer ses références facilement et les exporter/intégrer dynamiquement dans des documents&nbsp;;
- *synchroniser*&nbsp;: synchroniser ses références avec un compte en ligne&nbsp;;
- *collaborer*&nbsp;: échanger ses références avec d'autres utilisateurs.

===b===

## 1.3. Un logiciel libre
Zotero est logiciel libre qui peut être téléchargé puis installé sur tous les ordinateurs.

Il faut également utiliser un _connecteur_ pour récupérer des références sur le web puis faire le lien avec le logiciel qui les conserve.

===d===

## 2. Utilisation de Zotero

===b===

## 2.1. Utilisation générale

1. récupérer des références depuis un navigateur web (grâce à l'extension Zotero)&nbsp;;
2. une icône apparaît pour enregistrer une référence (plusieurs types de documents)&nbsp;;
3. la référence est enregistrée dans le logiciel Zotero sous la forme d'une notice&nbsp;;
4. il est possible de compléter la notice s'il manque des informations&nbsp;;
5. la synchronisation permet de sauvegarder les données en ligne (pour cela il faut créer un compte Zotero).

===b===

## 2.2. Exemple à partir du catalogue de la Bibliothèque universitaire de l'UdeM
Démonstration !

===b===

## 2.3. De nombreuses bases de données compatibles
De nombreux catalogues, bases de données, revues en ligne, journaux, blog, réseaux sociaux proposent des références&nbsp;:

- Sudoc
- Gallica
- OpenEdition, revues.org, Hypothèses
- Érudit
- Google Livres, Google Scholar, YouTube
- Internet Archive
- Cairn.info
- etc.

===b===

## 2.4. Description d'une utilisation classique
Prérequis&nbsp;: logiciel installé et extension installée (par exemple dans Firefox).

1. ouvrir le logiciel (Zotero Standalone)&nbsp;;
2. rechercher un document sur le web&nbsp;;
3. enregistrer la référence via l'extension du navigateur&nbsp;;
4. vérifier dans le logiciel si la référence convient (et si elle est placée au bon endroit)&nbsp;;
5. compléter les informations.

===b===

## 2.5. Quelques points importants

- pas besoin d'être en ligne pour _utiliser_ Zotero (mais vous ne pourrez pas afficher de pages web ou synchroniser vos collections)&nbsp;;
- les fonctions de Zotero sont nombreuses, n'hésitez pas à les tester et à les utiliser !
- Zotero permet de conserver et de gérer des références de pages web, mais ce n'est pas sa fonction première&nbsp;;
- n'oubliez pas d'ouvrir le logiciel Zotero pour enregistrer vos références.

===b===

## 2.7. Collecter

- via la navigateur en cliquant sur l'icône&nbsp;: Zotero récupère les métadonnées de la page web (page web qui présente un document)&nbsp;;
- si le texte intégral est disponible en PDF, Zotero le récupère et le sauvegarde&nbsp;;
- si la page web contient plusieurs références (par exemple une page de résultats), alors l'icône est un dossier et il est possible de sélectionner les résultats que vous souhaitez conserver&nbsp;;
- Zotero peut ajouter une référence automatiquement grâce à un identifiant – ISBN ou DOI&nbsp;: cliquez sur "Ajouter un élément par son identifiant" dans la barre d'outils Zotero, puis tapez le numéro d'identification, l'élément sera ajouté à votre bibliothèque.

===b===

## 2.6. Créer
Vous pouvez aussi créer manuellement des références en sélectionnant le type de document que vous souhaitez référencer et en remplissant les champs de la notice.

===d===

## 3. Organiser

===b===

## 3.1. Collections

- dans colonne de gauche, "Mes collections" ou "Ma bibliothèque", il est possible de créer plusieurs collections&nbsp;;
- créez une nouvelle collection en cliquant sur le bouton "Nouvelle collection"&nbsp;;
- il est possible de créer, renommer ou supprimer une collection (via un clic droit sur une collection)&nbsp;;
- pour organiser vos références il suffit de les déplacer d'un dossier d'une collection à une autre&nbsp;;
- les collections peuvent comprendre des sous-collections !

===b===

## 3.2. Tags et marqueurs

- les tags et les marqueurs servent à ajouter des informations de description&nbsp;;
- vous pouvez vous créer votre propre système de marqueurs et de tags pour organiser plus finement vos références&nbsp;;
- certaines références importées comportent parfois des marqueurs, vous pouvez vous en inspirer ou les supprimer&nbsp;;
- une recherche dans Zotero interroge tous les champs, y compris les marqueurs et les tags !

===b===

## 3.3. Champ "note"

- il permet de saisir du texte libre lié à une référence précise&nbsp;;
- il peut y avoir plusieurs notes pour une même référence&nbsp;;
- il est possible de les visualiser rapidement, de les supprimer, et elles sont synchronisées si vous avez un compte Zotero&nbsp;;
- une recherche dans Zotero interroge tous les champs, y compris les notes !

===d===

## 4. La recherche avancée
Vous pouvez rechercher dans toutes vos références en cliquant sur la loupe, vous accédez alors à une interface de recherche avancée.

![](images/zotero-recherche-avancee.png)

===d===

## 5. Création de bibliographies

===b===

## 5.1. Comment créer une bibliographie ?

- pour citer une référence ou créer une bibliographie, il faut utiliser un style ou une norme&nbsp;;
- il existe de nombreux styles de bibliographie (https://github.com/citation-style-language/styles et https://www.zotero.org/styles), dont certains sont déjà intégrés à Zotero&nbsp;;
- par défaut je vous conseille d'utiliser le style suivant&nbsp;: "American Psychological Association 6th edition"&nbsp;;
- pour modifier le style par défaut de Zotero&nbsp;: Édition > Préférences > Citer&nbsp;;
- chaque référence ou groupe de références peut être récupéré sous différents formats pour être intégré dans un document via un traitement de texte&nbsp;;
- plus d'informations&nbsp;: https://www.zotero.org/support/fr/creating_bibliographies
- démonstration !

===b===

## 5.2. Citer et créer une bibliographique directement dans LibreOffice
Zotero peut s'intégrer dans des outils comme les traitements de texte.
Cela vous permet de citer des références et de créer des bibliographie plus facilement :

- ouvrez LibreOffice et vérifiez que vous disposez bien des options de Zotero (dans la partie supérieure gauche) ;
- si vous n'avez pas l'extension Zotero pour LibreOffice, installez là via le logiciel Zotero : Édition > Préférences > Citer > Traitements de texte > Installer le module LibreOffice
- vous pouvez ajouter une citation d'une référence préalablement enregistrée dans Zotero ;
- une fois des citations ajoutées, vous pouvez créer une bibliographie de ces citations ;
- si vous modifier vos références dans Zotero, vous pouvez mettre à jour votre document LibreOffice.

===d===

## 6. Création de compte

===b===

## 6.1. Fonctionnalités proposées avec un compte
Créer un compte sur Zotero permet plusieurs fonctionnalités&nbsp;:

- synchroniser ses références avec un compte en ligne&nbsp;: elles sont sauvegardées et disponibles en se connectant depuis n'importe quel ordinateur&nbsp;;
- utiliser l'interface web pour ajouter ou modifier vos références (moins pratique qu'avec le logiciel)&nbsp;;
- rendre public et visible vos références pour les partager avec d'autres personnes&nbsp;;
- échanger des références et en importer depuis d'autres comptes&nbsp;;
- créer ou participer à des groupes&nbsp;;
- pour créer un compte (gratuit, aucun message envoyé par Zotero)&nbsp;: https://www.zotero.org/user/register

===b===

## 6.2. Créez-vous un compte !

===d===

## 7. Création et gestion de groupes

===b===

## 7.1. À quoi sert un groupe ?
Un groupe est une collection à laquelle plusieurs personnes peuvent participer&nbsp;:

- ajouter des références pour les rassembler dans un même groupe&nbsp;;
- plusieurs personnes peuvent contribuer à un même groupe&nbsp;;
- plusieurs rôles dans un même groupe&nbsp;: consulter, créer, modifier, administrer&nbsp;;
- le groupe peut être public avec des permissions plus ou moins ouvertes&nbsp;;
- _un groupe est facilement accessible via l'API de Zotero_.

===d===

## 8. BibTeX
BibTeX est un format de fichier structurant une bibliographie :

- format qui a été créé en 1985 pour gérer les bases de données bibliographiques dans des fichier LaTeX ;
- le format BibTeX fonctionne par entrée bibliographiques : chaque entrée correspond à une description d’un document avec des items “mot-clef = valeur”.

```
@book{kirschenbaum_track_2016,
  address = {{Cambridge, Massachusetts, Etats-Unis d'Am{\'e}rique}},
  title = {Track Changes: A Literary History of Word Processing},
  isbn = {978-0-674-41707-6},
  shorttitle = {Track Changes},
  language = {anglais},
  publisher = {{The Belknap Press of Harvard University Press, 2016}},
  author = {Kirschenbaum, Matthew G.},
  year = {2016, cop. 2016}
}
```

===d===

## 9. Better BibTeX

===b===

## 9.1. Installation de Better BibTex
Better BibTex (https://retorque.re/zotero-better-bibtex/installation/) ajoute à Zotero des fonctionnalités très pratiques :

- affichage des clés de citation ;
- nouveaux formats d'export ;
- exports dynamiques (mises à jour automatiques) de bibliographies.

===b===

## 9.2. Installez Better BibTeX
[https://retorque.re/zotero-better-bibtex/installation/](https://retorque.re/zotero-better-bibtex/installation/)

===d===

## Colophon
CC BY-SA  
Contact : Antoine Fauchié, Université de Montréal ([antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca))  
Outils utilisés pour cette présentation : Markdown, Reveal.js

<small>Source : [https://gitlab.huma-num.fr/ecrinum/manuels/introduction-a-zotero/](https://gitlab.huma-num.fr/ecrinum/manuels/introduction-a-zotero/)</small>
